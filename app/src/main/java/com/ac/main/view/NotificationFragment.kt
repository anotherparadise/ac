package com.ac.main.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ac.R
import com.ac.view.GlideApp
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : Fragment() {

    companion object {
        fun newInstance(): NotificationFragment {
            return NotificationFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        GlideApp.with(activity!!)
            .load("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSU2lYdZo1beJjJYyIooQ5BIvyhqlhFd0xP8YLx57gSfxS8SEfr")
            .into(notification_image_view)
    }
}
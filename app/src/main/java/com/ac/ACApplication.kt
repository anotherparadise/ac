package com.ac

import android.app.Activity
import android.app.Application
import android.content.Context
import android.util.Log
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.ac.model.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ACApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)

        WorkManager.getInstance().enqueue(PeriodicWorkRequestBuilder<ACWorker>(5000, TimeUnit.MILLISECONDS).build())
    }

    class ACWorker(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {

        override fun doWork(): Result {
            Log.e("OK", "OK".repeat(20))
            return Result.success()
        }
    }
}
package com.ac.model.dao

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "post")
data class PostEntity(
    @PrimaryKey(autoGenerate = true) var id: Int? = null,
    var userId: Int? = null, var title: String? = null, var body: String? = null
)
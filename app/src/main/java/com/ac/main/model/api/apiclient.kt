package com.ac.main.model.api

import io.reactivex.Observable
import retrofit2.http.GET

interface IApiClient {

    @GET("posts")
    fun getPosts(): Observable<List<PostModel>>
}

data class PostModel(var userId: Int? = null, var id: Int? = null, var title: String? = null, var body: String? = null)
package com.ac.main.viewmodel

import androidx.lifecycle.MutableLiveData
import com.ac.ACApplication
import com.ac.main.model.api.PostModel
import com.ac.main.model.repository.MainRepository
import com.ac.viewmodel.DisposableViewModel
import com.ac.viewmodel.ViewModelData
import com.ac.viewmodel.setSuccess
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class DashboardViewModel(private val mainRepository: MainRepository, application: ACApplication) :
    DisposableViewModel(application), AnkoLogger {
    val postsLiveData = MutableLiveData<ViewModelData<List<PostModel>>>()

    fun getPost() {
        disposable.add(
            mainRepository.getPosts().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribeWith(object : DisposableObserver<List<PostModel>>() {
                    override fun onComplete() {
                        info("COMPLETED".repeat(10))
                    }

                    override fun onNext(t: List<PostModel>) {
                        postsLiveData.setSuccess(t)
                    }

                    override fun onError(e: Throwable) {
                        info(e.message?.repeat(20))
                    }
                })
        )
    }
}
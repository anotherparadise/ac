package com.ac.viewmodel

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ac.ACApplication
import io.reactivex.disposables.CompositeDisposable

sealed class DataState {
    object LOADING : DataState()
    object SUCCESS : DataState()
    object ERROR : DataState()
}

data class ViewModelData<out T> constructor(val dataState: DataState, val data: T? = null, val message: String? = null)

fun <T> MutableLiveData<ViewModelData<T>>.setSuccess(data: T) = postValue(ViewModelData(DataState.SUCCESS, data))

fun <T> MutableLiveData<ViewModelData<T>>.setLoading() = postValue(ViewModelData(DataState.LOADING, value?.data))

fun <T> MutableLiveData<ViewModelData<T>>.setError(message: String) =
    postValue(ViewModelData(DataState.ERROR, value?.data, message))

open class DisposableViewModel(application: ACApplication) : AndroidViewModel(application) {

    protected val disposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()
    }
}
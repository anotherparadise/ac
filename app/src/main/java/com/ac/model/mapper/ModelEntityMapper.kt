package com.ac.model.mapper

import com.ac.main.model.api.PostModel
import com.ac.model.dao.PostEntity
import org.mapstruct.Mapper

@Mapper
interface ModelEntityMapper {
    fun postEntityToModel(post: PostEntity): PostModel
    fun postModelToEntity(post: PostModel): PostEntity
}
package com.ac.main.model.repository

import com.ac.main.model.api.IApiClient

class MainRepository(private val apiClient: IApiClient) {

    fun getPosts() = apiClient.getPosts()

}
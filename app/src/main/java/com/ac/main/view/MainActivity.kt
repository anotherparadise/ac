package com.ac.main.view

import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.ac.R
import com.ac.view.InjectableFragmentActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : InjectableFragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //TODO need to find a way to change CoroutineContext
        /* GlobalScope.launch(
             EmptyCoroutineContext, CoroutineStart.DEFAULT, {
             }
         )*/

        setupNavigation()
    }

    override fun onSupportNavigateUp() = findNavController(R.id.main_nav_host_fragment).navigateUp()

    private fun setupNavigation() {
        //        NavigationUI.setupActionBarWithNavController(this, navController)
        NavigationUI.setupWithNavController(
            bottom_navigation_view,
            Navigation.findNavController(this, R.id.main_nav_host_fragment)
        )
    }
}

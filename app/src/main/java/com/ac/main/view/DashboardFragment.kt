package com.ac.main.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ac.R
import com.ac.main.model.api.PostModel
import com.ac.main.viewmodel.DashboardViewModel
import com.ac.view.GlideApp
import com.ac.view.InjectableFragment
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.jetbrains.anko.AnkoLogger
import javax.inject.Inject

class DashboardFragment : InjectableFragment(), AnkoLogger {
    @Inject
    lateinit var dashboardViewModel: DashboardViewModel

    companion object {
        fun newInstance(): DashboardFragment {
            return DashboardFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dashboardViewModel.postsLiveData.observe(this, androidx.lifecycle.Observer {
            postRecyclerView.layoutManager = LinearLayoutManager(context)
            postRecyclerView.adapter = PostAdapter(it.data!!)
        })
        dashboardViewModel.getPost()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_dashboard, container, false)

    class PostAdapter(private val postList: List<PostModel>) : RecyclerView.Adapter<PostAdapter.ViewHolder>() {

        object PostOnClickListener : View.OnClickListener {
            override fun onClick(v: View) {
                Toast.makeText(v.context, "OK", Toast.LENGTH_LONG).show()
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val post = postList[position]
            GlideApp.with(holder.userProfileImageView.context)
                .load("""https://picsum.photos/500/400/?image=${post.id}""")
                .into(holder.userProfileImageView)
            val itemView = holder.itemView
            itemView.setOnClickListener(PostOnClickListener)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.dashboard_cardview, parent, false))
        }

        override fun getItemCount(): Int {
            return postList.size
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val userProfileImageView = itemView.findViewById(R.id.userProfileImageView) as ImageView
        }
    }
}

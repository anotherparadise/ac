package com.ac.main.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.ac.ACApplication
import com.ac.main.model.repository.MainRepository
import com.ac.main.view.DashboardFragment
import com.ac.main.viewmodel.DashboardViewModel
import com.ac.model.dao.ACDataBase
import com.ac.model.di.ViewModelKey
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [DashboardFragmentModule.ProvideViewModelModule::class])
abstract class DashboardFragmentModule {

    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun dashBoardFragment(): DashboardFragment

    @Module
    class ProvideViewModelModule {

        @Provides
        @IntoMap
        @ViewModelKey(DashboardViewModel::class)
        fun addViewModelIntoMap(mainRepository: MainRepository, application: ACApplication): ViewModel =
            DashboardViewModel(mainRepository, application)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun postDao(database: ACDataBase) = database.postDao()

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: DashboardFragment
        ) = ViewModelProviders.of(target, factory).get(DashboardViewModel::class.java)
    }
}
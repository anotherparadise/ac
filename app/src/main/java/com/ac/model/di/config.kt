package com.ac.model.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.ac.ACApplication
import com.ac.main.di.DashboardFragmentModule
import com.ac.main.model.api.IApiClient
import com.ac.main.model.repository.MainRepository
import com.ac.main.view.MainActivityModule
import com.ac.model.dao.ACDataBase
import com.ac.model.mapper.ModelEntityMapper
import com.ac.model.repository.ApplicationRepository
import dagger.BindsInstance
import dagger.Component
import dagger.MapKey
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import org.mapstruct.factory.Mappers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, UIModule::class])
interface AppComponent : AndroidInjector<ACApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: ACApplication): Builder

        fun build(): AppComponent
    }

    override fun inject(app: ACApplication)
}

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: ACApplication) = requireNotNull(application.applicationContext)

    @Provides
    @Singleton
    fun provideApplicationRepository() = ApplicationRepository()

    @Provides
    @Singleton
    fun provideDatabase(application: ACApplication) =
        Room.databaseBuilder(application, ACDataBase::class.java, "ac").build()

    @Provides
    @Singleton
    fun retrofit() = requireNotNull(
        Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .build()
    )

    @Provides
    fun mainRepository(apiClient: IApiClient) = MainRepository(apiClient)

    @Provides
    @Singleton
    fun apiClient(retrofit: Retrofit) = requireNotNull(retrofit.create(IApiClient::class.java))

    @Provides
    @Singleton
    fun modelEntityMapper() = requireNotNull(Mappers.getMapper(ModelEntityMapper::class.java))
}

@Module(includes = [MainActivityModule::class, DashboardFragmentModule::class])
class UIModule {

    @Suppress("UNCHECKED_CAST")
    @Provides
    @Singleton
    fun provideViewModelFactory(
        providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
    ) = object : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return requireNotNull(providers[modelClass]).get() as T
        }
    }
}

@MapKey
@Target(AnnotationTarget.FUNCTION)
annotation class ViewModelKey(
    val value: KClass<out ViewModel>
)